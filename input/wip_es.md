# Trabajo en proceso

Esta pagina no ha sido traducida a tu idioma aun :(

Puedes usar la barra desplegable de lenguajes para observar la pagina en otro idioma, sin embargo tambien puedes **[Contribuir](https://gitlab.com/pokemonsdk/wiki_sdk/-/blob/develop/CONTRIBUTING.md)** a la wiki.

