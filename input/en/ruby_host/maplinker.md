# MapLinker editor

The MapLinker is the `map connection` of PSDK. It currently only allow to link 4 maps to the current map (North, South, East, West).


## How to use the MapLinker ?

Here's a picture showing how to use the MapLinker
![Demo MapLinker](img/ruby_host/maplinker_tutorial.png "MapLinker Demo")

On this picture, 3 maps are linked to the edited map. The offset (to the right / bottom) calculation is done by comparing the **mandatory** 3 common (columns/rows).
- If the arrow goes to the same direction of the current offset, use a **positive** number to set the offset.
- If the arrow goes in the other direction of the current offset, use a **negative** number to set the offset.
- If you cannot draw an arrow, set 0.

![tip](info "The MapLinker editor sets the link for the linked map to the current edited map.")