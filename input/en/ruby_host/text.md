# Text editor

![tip](warning "This page comes from the **old Wiki**. It's probably not up to date.")

## Text editor UI

The `Texts` button allow you to access the System Text editor UI.

![Interface|center](img/ruby_host/Rubyhost_textmain.png "Text editor UI")

You can edit texts in the following languages :
- Japanese (without Kanjis)
- German
- English
- Spanish
- French
- Italian
- Korean

![tip](info "The maker needs to translate its own text (message) using the CSV system & Google Sheet.")

## Edit texts

1. In the `Text file :` dropdown, choose the file you want to edit.
    - If you want to add a text :
        - Click on `Add text`
        - Fill the field you want to set to the text
        - Click on `Validate`.  
            ![tip](warning "If you don't validate the changes are not taken in account.")
        - Click on `Save`.
    - If you want to modify a text :
        - In `TextID :`, enter the ID of the text you want to edit and click on `Goto`.
        - Edit the text in the desired languages
        - Click on `Validate`.  
            ![tip](warning "If you don't validate the changes are not taken in account.")
        - Click on `Save`.
    - If you want to remove the last text:
        - Click on `Remove last text`.
