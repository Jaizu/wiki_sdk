# Programming in PSDK

## Notice

Under PSDK the programming methods are a bit different than under classic RMXP Project. PSDK uses Ruby 2.5+ while RMXP Project uses Ruby 1.8. PSDK also has a separate script base (from the user base) because PSDK relies on Ruby files instead of a big monolithic Scripts.rxdata (this helps PSDK to work with GIT).

![tip](info "Ruby 2.5 is at least 4 times faster than Ruby 1.8 and have a lot of new cool feature comparing to Ruby 1.8.")

It's strongly suggested to know a bit how to program in Ruby before programming in PSDK. For that you have two tutorials :
- [Tutorials Point](https://www.tutorialspoint.com/ruby/index.htm)
- [Nuri Yuri (translated)](https://gitlab.com/NuriYuri/tutoriel_ruby/blob/master/en/0_Table_of_Contents.md)

![tip](danger "Do not follow in any case the Essentials Script tutorial, even if Marin or Lukas did them. They're potentially bad because Essentials doesn't follow the basis of Ruby Programming (using snake case for example) and runs under Ruby 1.8 which lead to bad implementations in Ruby 2.5.")

## Index

In this section of the Wiki we'll see various topic so you'll need to be on this index to access to the index of the other topic (the page selector won't show the topics).

Here's all the topic that will be done in the Programming section :
- [Set the programming environment up](en/programming/env/index.md)
- [Some coding rules](en/programming/scr.md)
- [How does it work](en/programming/hdiw/index.md) : This section will explain various PSDK system.