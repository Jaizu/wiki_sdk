# Create a Trainer event

![tip](warning "This page comes from the **old Wiki**. It's probably not up to date.")

In PSDK a trainer consist of 3 event pages

## First Page

The first page is related to the player detection, call `trainer_spotted(x)` in a condition where `x` is the number of steps the event can do to detect the player.

![Page 1|center](img/event_making/Psdk_trainer_1.png "First event page")

## Second page

The second page is the trainer speech when he spotted the player.  
You'll have to specify the Ruby Host trainer ID using the variable `8` and then start one of the 5 first RMXP battle.

If you don't use the RMXP defeat command, use the switch `37` to be sure the player won and enable the 3rd page.

![Page 2|center](img/event_making/Psdk_trainer_2.png "Battle start")

## Third page

The third page is only what the trainer will say if you speak to him again. (Can just say a dialog or start a new battle if wanted).
![Page 3|center](img/event_making/Psdk_trainer_3.png "Third page")
