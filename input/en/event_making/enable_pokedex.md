# Enable the Pokedex

![tip](warning "This page comes from the **old Wiki**. It's probably not up to date.")

## Enable the Pokedex

Using event command: Enable switch `100`.  
Using script:
```ruby
$pokedex.enable
```

## Disable the Pokedex

Using event command: Disable switch `100`.  
Using script:
```ruby
$pokedex.disable
```

## Knowing the activation state of the Pokedex

Using event command: Read switch `100`.  
Using script:
```ruby
$pokedex.enabled?
```

## Define the national or regional state of the Pokedex

Using event command: Edit switch `99`. Enabled = national, disabled = regional.
Using script:
```ruby
$pokedex.set_national(is_national)
```

Parameters :
- `is_national` : `true` if the dex should be national, `false` otherwise.

## Detect if the Pokedex is in national mode

Using event command: Read switch `99`.  
Using script:
```ruby
$pokedex.national?
```

## Knowing how many Pokémon the player has seen

Using event command: Read variable `2`.  
Using script:
```ruby
$pokedex.pokemon_seen
```

## Knowing how many Pokemon have been caught

Using event command: Read variable `3`.  
Using script:
```ruby
$pokedex.pokemon_captured
```

## Other informations

You can know a lot of things by accessing the $pokedex variable. Everything is documented here : [`PFM::Pokedex`](https://psdk.pokemonworkshop.fr/yard/PFM/Pokedex.html)
