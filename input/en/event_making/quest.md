# Managing Quests

![tip](warning "This page is about managing quests, not creating them. To learn how to create quests go [here](https://psdk.pokemonworkshop.fr/wiki/en/ruby_host/quest.html).")

In PSDK you manage quests through `$quests`. All the methods are explained in the documentation : [PFM::Quests](https://psdk.pokemonworkshop.fr/yard/PFM/Quests.html).

We'll see how to use this variable

## 1st Step : Start Quests

To start a quest, call `$quests.start(id)` where `id` is the id of the quest in Ruby Host.

Once `$quests.start(id)` was called you need to tell the System to check started quests:
```ruby
$quests.check_up_signal
```

This method checks the quest from start to finish. It also shows the quest name in the screen.

![Quest informer|center](https://i.imgur.com/X6OI8Zo.png "Starting a new quest")

## 2nd step : Validate the Goal

There are various methods to validate goals:
- `$quests.beat_npc(quest_id, npc_name_index)` where `quest_id` is the quest id and `npc_name_index` is the index of the name of the NPC to beat in the goal order of Ruby Host
- `$quests.speak_to_npc(quest_id, npc_name_index)` where the parameters are the same

The other objectives are automatically validated by PSDK.

![tip](warning "You should call `$quests.check_up_signal` at the end of the event that validates the goals.")

## 2nd Step Continued : Showing a Hidden Goal

To show a hidden goal call:
```ruby
$quests.show_goal(quest_id, goal_index)
```

The parameters are: 
- `quest_id` the quest id.
- `goal_index` the index of the goal in the goal order.

![tip](info "You should only hide the "Speak to an NPC" objectives!")

## 3rd Step: Check If the Quest Is Finished and Give the Reward

To finish a quest you have two things to do, check if all the goals are done & distribute the reward.

![tip](warning "If you don't distribute the reward, the quest book will not consider the quest as finished.")

### Check If All the Goals Are Done

To check if all the goals are done, write the following line in a condition:
```ruby
$quests.finished?(quest_id)
```

### Check If the Rewards Were Distributed

To check if the rewards were distributed write this line in a condition:
```ruby
$quests.earnings_got?(quest_id)
```

To distribute them use the following command:
```ruby
$quests.get_earnings(quest_id)
```

![tip](info "It's recommanded to check if the quest is finished before checking if the rewards were distributed.")
