# Créer un centre Pokémon

![tip](warning "Cette page provient de l'Ancien Wiki. Elle n'est peut-être pas à jour.")

Sur cette page nous allons aborder l'évent Making d'un centre Pokémon. Nous considérons que le Mapping de celui-ci est acquis.

## Le tout première évènement : L'infirmière

![Infirmière|right](img/event_making/Psdk_centre_01.png "Evènement de l'infirmière")
Dans un centre Pokémon sous PSDK il est extrêmement important que l'infirmière Joëlle. Quand vous êtes mis Hors Combat et que le Système vous renvoie au centre Pokémon, il faut que les évènement communs puissent automatiquement trouver l'infirmière et la façon la plus simple c'est que ce soit l'évènement d'ID 001. Cet évènement est assez simple, il se contente d'appeler l'évènement commun `4` "**Centre - Infirmière**"

## Les machines de soin

![Machine Gauche Page 1|left](img/event_making/Psdk_centre_02.png "Machine de gauche page 1")
![Machine Droite Page 1|right](img/event_making/Psdk_centre_03.png "Machine de droite page 1")
Il y a une machine de soin dans le centre Pokémon mais comme elle prend deux tiles de large ça en fait deux. L'évènement commun gère la commutation des interrupteurs et variables permettant à la machine de lancer son animation. Je vous laisse en image la configuration des machines de soin.

![Machine Gauche Page 2|left](img/event_making/Psdk_centre_04.png "Machine de gauche page 2")
![Machine Droite Page 2|right](img/event_making/Psdk_centre_05.png "Machine de droite page 2")

## Le PC de stockage

![PC|right](img/event_making/Psdk_centre_06.png "PC de Stockage")
Le PC de stockage est assez simple, c'est juste un évènement qui contient une commande de script `demarrer_pc`. 

## L'évènement de sortie du centre

L'évènement de sortie du centre est assez important, il permet à PSDK de mémoriser le position du dernier soin réalisé à l'aide de l'infirmière uniquement si vous passez par ce centre :

![Sortie|center](img/event_making/Psdk_centre_07.png "Evènement de sortie du centre")

![tip](info "Lorsque vous modifiez l'activation du MapLinker (interrupteur 6), faites le toujours avant de téléporter le héros !")
