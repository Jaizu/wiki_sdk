# Créer un dresseur

![tip](warning "Cette page provient de l'Ancien Wiki. Elle n'est peut-être pas à jour.")

Dans PSDK, un dresseur consiste en généralement 3 pages.

## Première page

La première page est la détection du joueur, à l'aide de la commande `trainer_spotted(x)` où `x` est le nombre de pas que l'évènement a à faire pour atteindre le joueur. (La commande ne détecte pas derrière les obstacles).

![Page 1|center](img/event_making/Psdk_trainer_1.png "Première page de l'évènement de dresseur")

## Deuxième page

La deuxième page est le déroulement de l'évènement lorsqu'il vous a détecté, vous devrez définir l'ID du dresseur de Ruby Host (bouton dresseurs) que vous voulez combattre à l'aide de la variable `8` puis lancer un combat régulier d'RMXP.

Si vous n'utilisez pas la condition de défaite, entrez une condition sur l'interrupteur `37` pour être certain que vous sortez victorieux du combat et activer la troisième page du dresseur

![Page 2|center](img/event_making/Psdk_trainer_2.png "Page de lancement du combat")

## Troisième page

La troisième page est juste un dialogue, vous êtes libres de faire comme vous le souhaitez.
![Page 3|center](img/event_making/Psdk_trainer_3.png "Troisième page de l'évènement de dresseur")
