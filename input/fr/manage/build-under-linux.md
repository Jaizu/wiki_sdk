# Compiler sous Linux

Si vous êtes un utilisateur Linux et en particulier de systèmes basés sur Debian, nous vous recommandons de compiler les dépendances de PSDK manuellement.

Voici ce que vous avez à faire:

## Installer les outils nécessaires
Pour installer et compiler les dépendances de PSDK, vous aurez besoin d'exécuter les commandes suivantes.
Note : Si vous avez un problème avec « sudo gem install rake rake-compiler », essayez sans sudo.
```
cd
sudo apt-get install git
git clone https://gitlab.com/NuriYuri/PSDK-DEP.git
git clone https://github.com/NuriYuri/Ruby-Fmod.git
git clone https://github.com/NuriYuri/LiteRGSS

sudo apt-get install -y build-essential
sudo apt-get install -y ruby ruby-dev
sudo gem install rake rake-compiler

ruby -v
# ruby -v devrait retourner une version égale ou plus récente que Ruby 2.5.0
# Si ce n'est pas le cas, suivez le tutoriel sur cette page : https://www.hugeserver.com/kb/install-ruby-rails-debian-ubuntu/
# Mais à la place d'installer Ruby 2.4, choisissez Ruby 2.5.x 

sudo dpkg -i PSDK-DEP/Fmod-for-PSDK.deb
sudo dpkg -i PSDK-DEP/SFML-2.5.deb

cd Ruby-Fmod
rake clean
rake compile
cd ..

cd LiteRGSS
rake clean
sudo rake compile
cd ..
```

## Créer un projet PSDK
Pour créer un projet PSDK, vous aurez besoin de télécharger le fichier 7z de PSDK se terminant par “Linux” et de l'extraire dans un dossier modifiable grâce au package `p7zip`.

Attention, ce n'est pas tout !
Vous avez maintenant besoin de copier les fichiers `~/LiteRGSS/lib/LiteRGSS.so` et `~/Ruby-Fmod/lib/RubyFmod.so` à la racine de votre projet (où l'on peut trouver Game.rb).

## Lancer PSDK sans terminal
Quelqu'un a créé un launcher : [Game](https://www.mediafire.com/file/g6480yd31e4bgby/Game/file)

## Mettre à jour PSDK
Utilisez cet outil : [Updater](https://www.mediafire.com/file/0g77n4lul4n272r/Update/file)

