# Créer une classe de Dresseur

Vous avez probablement déjà lu le [tutoriel à propos des Dresseurs de PSDK](trainer.html) et vous souhaitez changer la classe de votre dresseur ou vous avez eu cette erreur, 

![TrainerError](img/ruby_host/fr/trainer_error.png)

Dans ce cas, suivez ce tutoriel.

## Ajouter une classe de Dresseur pour un nouveau Dresseur (et résoudre l'erreur ci-dessus)

Ouvrez RubyHost dans l'onglet Textes:

![TextsTab](img/ruby_host/fr/texts.png)

Choisissez ``Classes de Dresseurs`` (ID 29) dans la liste en haut à gauche:

![TrainerClass](img/ruby_host/fr/trainer_class.png)

Cliquez sur ``Ajouter un texte`` et remplissez les champs à votre disposition en fonction des langues de votre jeu: 

![Fill](img/ruby_host/fr/fill.png)

## Modifier la classe d'un dresseur existant

Sélectionnez l'ID du dresseur (ici 127):

![ID127](img/ruby_host/fr/id127.png)

Remplacez les noms des classes dans les champs associés selon les langues que vous utilisez pour votre jeu.

![Blankfields](img/ruby_host/fr/fields.png)

![tip](warning "N'oubliez jamais de ``valider l'édition`` ET de ``sauvegarder`` à chaque manipulation pour qu'elle soit appliquée.")