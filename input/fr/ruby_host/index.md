# Ruby Host

![tip](warning "Cette page provient de l'Ancien Wiki. Elle n'est peut-être pas à jour.")

RubyHost est le nom de l'éditeur des Bases de Données de Pokémon SDK. Grâce à une interface intuitive et simple d'utilisation développée grâce à Visual Studio 2012, il permet la gestion de nombreuses séries de données telles que celles des Pokémon, des Objets et des Zones.

![Accueil de Ruby Host|center](img/ruby_host/Rubyhost_main.png "L'interface d'accueil de RubyHost")

## Les éditeurs

Ruby Host propose divers éditeurs permettant de réaliser divers modifications de données :

- [Editeur de Pokémon](pokemon.md)
- [Editeur d'Attaque](move.md)
- [Editeur d'Objets](item.md)
- [Editeur de Zone](zone.md)
- [Editeur de Textes](text.md)
- [Editeur de Type](type.md)
- [Editeur de Dresseurs](trainer.md)
- [Editeur de Quêtes](quest.md)
- [Map Linker](maplinker.md)

## Les tutoriels additionnels

- [Créer une classe de Dresseur](create_trainer_class.md)

![tip](info "L'éditeur de Talent n'existe pas, ce bouton sert juste à générer le tableau des talents pour qu'ils aient le bon texte dans PSDK. Tous les talents sont scriptés.")