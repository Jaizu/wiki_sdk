# Editeur de Pokémon

![tip](warning "Cette page provient de l'Ancien Wiki. Elle n'est peut-être pas à jour.")

## Interface des Pokémon de RubyHost

Le bouton "Pokémon" accessible sur l'interface d'accueil de RubyHost permet d'accéder à une fenêtre permettant de modifier les données des Pokémon, la liste des Capacités qu'ils apprennent par montée de niveau, par reproduction ou par les Capsules Techniques et Spéciales (CT/CS).

![Vue de l'editeur|center](img/ruby_host/Rubyhost_pokemonmain.png "Interface principale de l'éditeur de Pokémon")

## Créer un nouveau Pokémon

Pour créer un nouveau Pokémon :

1. Assurez vous d'avoir correctement [ajouté les entrées de texte](text.md) qui lui correspondent. (Nom, Description, Espèce)
2. Cliquez sur `Ajouter un Pokémon` sur l'interface d'édition des Pokémon.
3. Vérifiez que les textes dans `Nom :`, `Espèce :` et `Description :` correspondent.
4. Paramétrez les différentes données du Pokémon (Hauteur, Poids, Types, Statistiques, etc.).  
    ![tip](info "Le bouton `Vérifier l'évolution spéciale` permet de vérifier que vous avez bien paramétré votre évolution. Une fenêtre s'ouvre pour vous donner le résultat de votre paramétrage.")
5. Cliquez sur `Éditeur de moveset`. Une fenêtre s'ouvre et vous permet de gérer la liste des Capacités apprises (par montée de niveau) par le Pokémon en question.
    - Cliquez sur `Nouveau` dans `Niveaux :` ou `Attaques apprises :`, sélectionnez l'attaque dans `Attaques dans la Base de données :` et cliquez sur `Ajouter`.
    - N'oubliez pas de cliquer sur `Sauvegarder`.  
        ![Editeur de Moveset|center](img/ruby_host/Rubyhost_pokemonmovepool.png "Editeur de moveset")
6. Cliquez sur `Éditeur de CT / breedmove et objet porté`. Une fenêtre s'ouvre et vous permet de gérer la liste des CT et CS que peut apprendre le Pokémon, ainsi que les objets qu'il peut porter à l'état sauvage, la liste des attaques qu'il apprend par reproduction et les capacités qu'il peut apprendre par les [Maîtres des capacités](http://www.pokepedia.fr/Ma%C3%AEtre_des_Capacit%C3%A9s).
    - Sélectionnez les attaques et/ou CT/CS (etc.) dans les listes déroulantes et cliquez sur `Ajouter`.
    - N'oubliez pas de cliquer sur `Sauvegarder`.  
        ![Editeur de CT / breedmoves & objets|center](img/ruby_host/Rubyhost_pokemonctbreeditem.png "Éditeur de CT / breedmove et objet porté")
7. Une fois toutes les modifications apportées, cliquez sur `Sauvegarder`.

## Paramétrer l'évolution d'un Pokémon

Lorsque vous êtes sur la fenêtre d'édition des Pokémon, vous avez accès à une partie `Evolution :`. Celle-ci vous permet de paramétrer l'évolution de votre Pokémon. Voici la liste des différents types d'évolution disponible sur PSDK (la liste sera modifiée si besoin) et qui seront à rentrer dans `Spéciale : (Array de Hash)` :

- Évolution par niveau (`:min_level` et `:max_level`).
- Évolution par échange (`:trade_with`, `:trade`).
- Évolution par pierre (`:stone`).
- Évolution en tenant un objet spécifique (`:item_hold`).
- Évolution par bonheur (`:min_loyalty` et `:max_loyalty`).
- Évolution par attaque apprise (`:skill_X` avec `X` allant de 1 à 4).
- Évolution selon la météo (`:weather`).
- Évolution selon la position du joueur sur un tag spécifique (`:env`).
- Évolution par genre (`:gender`).
- Évolution selon le moment de la journée (`:day_night`).
- Évolution par appel de fonction (`:func`).
- Évolution selon la map où se trouve le joueur (`:maps`).

Certaines évolutions sont paramétrées dans les scripts.

Voici des exemples pris sur certains Pokémon officiels :

### L'évolution naturelle de Bulbizarre

Dans `Naturelle (ID) :`, rentrez 2 (l'ID de Herbizarre) et dans `Niveau :`, 16.

### L'évolution par échange de Machopeur

Laissez `Naturelle (ID) :` et `Niveau :` vides.  
Dans `Spéciale : (Array de Hash)`, entrez : 
```ruby
[ { :trade => 68 } ]
```
![tip](info "68 est l'ID de Mackogneur")

### L'évolution par pierre de Boustiflor

Laissez `Naturelle (ID) :` et `Niveau :` vides.  
Dans `Spéciale : (Array de Hash)`, entrez : 
```ruby
[ { :id => 71, :stone => 85 }]
```
![tip](info "71 est l'ID d'Empiflor et 85 l'ID de l'objet Pierre Plante")

### L'évolution en tenant un objet (et par échange) de Onix

Laissez `Naturelle (ID) :` et `Niveau :` vides.  
Dans `Spéciale : (Array de Hash)`, entrez : 
```ruby
[ { :trade => 208, :item_hold => 233 } ]
```
![tip](info "208 est l'ID de Steelix et 233 l'ID de l'objet Peau Métal")

![tip](info "Si vous souhaitez juste mettre une évolution par niveau en tenant un objet, il faudra remplacer `:trade => 208` par `:id => 208`")

### L'évolution par bonheur de Mélo

Laissez `Naturelle (ID) :` et `Niveau :` vides.  
Dans `Spéciale : (Array de Hash)`, entrez :
```ruby
[{:id=>35, :min_loyalty=>220}]
```

![tip](info "35 est l'ID de Mélofée et 220 le bonheur minimum")

![tip](info "Si vous souhaitez rajouter une borne supérieure de bonheur ajoutez avant `}]` : `, :max_loyalty=>X` où `X` sera le bonheur maximum")

### L'évolution par attaque apprise de Cochignon

Laissez `Naturelle (ID) :` et `Niveau :` vides.  
Dans `Spéciale : (Array de Hash)`, entrez :
```ruby
[ { :id => 473, :skill_1 => 246 } ]
```
![tip](info "473 est l'ID de Mammochon et 246 l'ID de l'attaque Pouvoir Antique")

### L'évolution par genre d'Apitrini

Laissez `Naturelle (ID) :` et `Niveau :` vides.  
Dans `Spéciale : (Array de Hash)`, entrez :
```ruby
[ { :gender => 2, :min_level => 21, :id => 416 } ]
```
![tip](info "2 est le genre Femelle, 21 est le niveau minimum et 416 l'ID d'Apireine")

### L'évolution selon le moment de la journée (et par bonheur) de Mentali

Laissez `Naturelle (ID) :` et `Niveau :` vides.  
Dans `Spéciale : (Array de Hash)`, entrez :
```ruby
[ { :id => 196, :min_loyalty => 220, :day_night => 3 } ]
```
![tip](info "196 est l'ID de Mentali, 220 le bonheur minimum, 3 le moment de la journée "Jour" (déterminé par la variable `::Yuki::Var::TJN_Tone`")

![tip](info "Si vous souhaitez retirez la condition du bonheur minimum, il suffit de supprimer `, :min_loyalty=>220` du Hash")

### L'évolution par appel de fonction de Babimanta

Laissez `Naturelle (ID) :` et `Niveau :` vides.  
Dans `Spéciale : (Array de Hash)`, entrez :
```ruby
[ { :id => 226, :func => :elv_demanta } ]
```
![tip](info "226 est l'ID de Démanta et `:elv_demanta` la fonction scriptée correspondante au code suivant : `def elv_demanta() $pokemon_party.has_pokemon?(223) end`")

### L'évolution selon la map où se trouve le joueur de Magnéton

Laissez `Naturelle (ID) :` et `Niveau :` vides.  
Dans `Spéciale : (Array de Hash)`, entrez :
```ruby
[ { :id => 462, :maps => [X] } ]
```
![tip](info "462 est l'ID de Magnézone et `X` l'ID de la map choisie")

## Paramétrer la méga-évolution d'un Pokémon

Lorsque vous êtes sur la fenêtre d'édition des Pokémon, vous avez accès à une partie `Evolution :`. Celle-ci vous permet de paramétrer l'évolution de votre Pokémon, ainsi que sa Méga-Évolution :

1. Sélectionnez le Pokémon dont vous voulez paramétrer la Méga-Évolution dans `Pokémon :`.
2. Sélectionnez l'entrée 30 ou 31 dans `Forme édité :`, qui correspond à la Méga-Évolution 1 ou 2.
3. Dans `Spéciale : (Array de Hash)`, rentrez `[{:gemme=>X}]` (où X correspond à l'ID de la Méga-Gemme dans la base de données des objets).
    Si vous devez créer une Méga-Gemme, référez-vous à ce tutoriel : [Créer un nouvel objet](item.html#cat_2).

N'oubliez pas de `Sauvegarder`.